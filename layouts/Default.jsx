import Head from 'next/head'

const DefaultLayout = ({ children }) => (
  <>
    <Head>
      <title>Total Seguros</title>
      <meta charSet="utf-8" />
    </Head>
    <div>{children}</div>
  </>
)
export default DefaultLayout