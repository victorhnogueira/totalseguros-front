import { Fragment, useState } from "react";
import Link from "next/link";
import axios from "axios";

import { Dialog, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import { logout } from "../utils/logout";

function SidemenuCreateClient({ open, setOpen }) {
  const [isLoadingRequest, setIsLoadingRequest] = useState(false);
  const [fullName, setFullName] = useState("");
  const [mail, setMail] = useState("");
  const [cpfcnpj, setCpfcnpj] = useState("");
  const [password, setPassword] = useState("");
  const [serverFeedback, setServerFeedback] = useState({
    active: false,
    error: false,
    msg: "",
  });

  function createClient(event) {
    event.preventDefault();
    setIsLoadingRequest(true);

    const instance = axios.create({
      baseURL: "https://totalseguros-api.herokuapp.com",
    });

    instance({
      method: "post",
      url: "/auth/client/register",
      data: {
        fullName,
        email: mail,
        cpfCnpj: cpfcnpj,
        password,
      },
    })
      .then(() => {
        setServerFeedback({
          active: true,
          error: false,
          msg: "Salvo com sucesso",
        });
      })
      .catch(() => {
        setServerFeedback({
          active: true,
          error: true,
          msg: "Erro ao salvar dados",
        });
      })
      .finally(() => {
        setIsLoadingRequest(false);
      });
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden z-50"
        open={open}
        onClose={setOpen}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>
          <div className="fixed inset-y-0 right-0 pl-10 max-w-full flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="relative w-screen max-w-md">
                <Transition.Child
                  as={Fragment}
                  enter="ease-in-out duration-500"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in-out duration-500"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <div className="absolute top-0 left-0 -ml-8 pt-4 pr-2 flex sm:-ml-10 sm:pr-4">
                    <button
                      className="rounded-md text-gray-300 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                      onClick={() => setOpen(false)}
                    >
                      <span className="sr-only">Fechar</span>
                      <XIcon className="h-6 w-6" aria-hidden="true" />
                    </button>
                  </div>
                </Transition.Child>
                <div className="h-full flex flex-col py-6 bg-white shadow-xl overflow-y-scroll">
                  <div className="px-4 sm:px-6">
                    <Dialog.Title className="text-lg font-medium text-gray-900">
                      Cadastrar novo cliente
                    </Dialog.Title>
                  </div>
                  <div className="mt-6 relative flex-1 px-4 sm:px-6">
                    {/* Replace with your content */}
                    <form
                      id="createClientForm"
                      className="space-y-1"
                      onSubmit={(e) => createClient(e)}
                    >
                      <div>
                        <label
                          htmlFor="inputFullName"
                          className="mb-1 text-xs text-gray-500"
                        >
                          Nome completo
                        </label>
                        <input
                          id="inputFullName"
                          className="w-full px-4 py-2 border border-gray-300 rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none ring-inset"
                          type="text"
                          name="fullName"
                          placeholder="Nome completo"
                          onChange={(e) => setFullName(e.target.value)}
                          value={fullName}
                          required
                        />
                      </div>
                      <div>
                        <label
                          htmlFor="inputMail"
                          className="mb-1 text-xs text-gray-500"
                        >
                          E-mail
                        </label>
                        <input
                          id="inputMail"
                          className="w-full px-4 py-2 border border-gray-300 rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none ring-inset"
                          type="email"
                          name="mail"
                          placeholder="E-mail"
                          onChange={(e) => setMail(e.target.value)}
                          value={mail}
                          required
                        />
                      </div>
                      <div>
                        <label
                          htmlFor="inputCpfCnpj"
                          className="mb-1 text-xs text-gray-500"
                        >
                          CPF/CNPJ
                        </label>
                        <input
                          id="inputCpfCnpj"
                          className="w-full px-4 py-2 border border-gray-300 rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none ring-inset"
                          type="text"
                          name="cpfCnpj"
                          placeholder="CPF/CNPJ"
                          onChange={(e) => setCpfcnpj(e.target.value)}
                          value={cpfcnpj}
                          required
                        />
                      </div>
                      <div>
                        <label
                          htmlFor="inputPassword"
                          className="mb-1 text-xs text-gray-500"
                        >
                          Senha
                        </label>
                        <input
                          id="inputPassword"
                          className="w-full px-4 py-2 border border-gray-300 rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none ring-inset"
                          type="password"
                          name="password"
                          placeholder="****"
                          onChange={(e) => setPassword(e.target.value)}
                          value={password}
                          required
                        />
                      </div>
                      {serverFeedback.active && (
                        <div
                          className={`flex items-center justify-between flex-shrink-0 p-2 bg-${
                            serverFeedback.error ? "red" : "green"
                          }-100`}
                        >
                          <p
                            className={`text-sm text-${
                              serverFeedback.error ? "red" : "green"
                            }-600`}
                          >
                            {serverFeedback.msg}
                          </p>
                        </div>
                      )}

                      <div className="py-4">
                        <button
                          type="submit"
                          className="flex items-center justify-center w-full px-4 py-2 text-sm text-white rounded-md bg-purple-700 hover:bg-purple-900 focus:outline-none focus:ring focus:ring-purple-700 focus:ring-offset-1 focus:ring-offset-white dark:focus:ring-offset-dark"
                          disabled={isLoadingRequest}
                        >
                          <span aria-hidden="true">
                            {isLoadingRequest && (
                              <svg
                                className="w-4 h-4 mr-2 animate-spin"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                              >
                                <path
                                  fillRule="evenodd"
                                  d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z"
                                  clipRule="evenodd"
                                />
                              </svg>
                            )}
                            {!isLoadingRequest && (
                              <svg
                                className="w-4 h-4 mr-2"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                              >
                                <path d="M5 4a2 2 0 012-2h6a2 2 0 012 2v14l-5-2.5L5 18V4z" />
                              </svg>
                            )}
                          </span>
                          <span>Cadastrar Cliente</span>
                        </button>
                      </div>
                    </form>
                    {/* /End replace */}
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

export default function AdminLayout({ children }) {
  const [isOpen, setIsOpen] = useState(false);
  const [createUserIsOpen, setCreateUserIsOpen] = useState(false);

  return (
    <>
      <SidemenuCreateClient
        open={createUserIsOpen}
        setOpen={setCreateUserIsOpen}
      />
      <div className="bg-gray-100 font-family-karla flex">
        <aside className="relative bg-indigo-500 h-screen w-64 hidden sm:block shadow-xl">
          <div className="p-6">
            <Link href="/">
              <a
                href="/"
                className="text-white text-3xl font-semibold uppercase hover:text-gray-300"
              >
                Total Seguros
              </a>
            </Link>

            <button
              onClick={() => setCreateUserIsOpen(!createUserIsOpen)}
              className="w-full bg-white cta-btn font-semibold py-2 mt-5 rounded-lg shadow-lg hover:shadow-xl hover:bg-gray-300 flex items-center justify-center"
            >
              <i className="fas fa-plus mr-3"></i> Novo cliente
            </button>
          </div>
          <nav className="text-white text-base font-semibold pt-3">
            <Link href="/">
              <a
                href="#"
                className="flex items-center active-nav-link text-white py-4 pl-6 nav-item"
              >
                <i className="fas fa-tachometer-alt mr-3"></i>
                Dashboard
              </a>
            </Link>
            <Link href="/clientes">
              <a
                href="/clientes"
                className="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item"
              >
                <i className="fas fa-sticky-note mr-3"></i>
                Clientes
              </a>
            </Link>
            <Link href="/seguros">
              <a
                href="/seguros"
                className="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item"
              >
                <i className="fas fa-table mr-3"></i>
                Seguros
              </a>
            </Link>
            <Link href="/publicacoes">
              <a
                href="/publicacoes"
                className="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item"
              >
                <i className="fas fa-tablet-alt mr-3"></i>
                Publicações
              </a>
            </Link>
          </nav>
          <a
            href="https://conselt.com.br"
            target="_blank"
            className="hidden absolute w-full upgrade-btn bottom-0 active-nav-link text-white flex items-center justify-center py-4"
          >
            <i className="fas fa-arrow-circle-up mr-3"></i>
            CONSELT
          </a>
        </aside>

        <div className="w-full flex flex-col h-screen overflow-y-hidden">
          <header className="w-full items-center bg-white py-2 px-6 hidden sm:flex">
            <div className="w-1/2"></div>
            <div
              x-data="{ isOpen: false }"
              className="relative w-1/2 flex justify-end"
            >
              <button
                onClick={() => setIsOpen(!isOpen)}
                className="realtive z-10 w-12 h-12 rounded-full overflow-hidden border-4 border-gray-400 hover:border-gray-300 focus:border-gray-300 focus:outline-none"
              >
                TS
              </button>
              <div
                className={`absolute w-32 bg-white rounded-lg shadow-lg py-2 mt-16 ${
                  !isOpen && "hidden"
                }`}
              >
                <button
                  onClick={() => logout()}
                  className="block w-full text-left px-4 py-2 hover:bg-gray-100"
                >
                  Sair
                </button>
              </div>
            </div>
          </header>

          <header
            className="w-full bg-sidebar py-5 px-6 sm:hidden"
          >
            <div className="flex items-center justify-between">
              <Link href="/">
                <a
                  href="/"
                  className="text-indigo-500 text-3xl font-semibold uppercase hover:text-gray-300"
                >
                  Total seguros
                </a>
              </Link>
              <button
              onClick={() => setIsOpen(!isOpen)}
              className="focus:outline-none">
                MENU
              </button>
            </div>

            <nav className={`flex flex-col pt-4 ${isOpen ? "flex" : "hidden"}`}>
              <Link href="/">
                <a
                  href="/"
                  className="flex items-center active-nav-link py-2 pl-4 nav-item"
                >
                  <i className="fas fa-tachometer-alt mr-3"></i>
                  Dashboard
                </a>
              </Link>
              <button
                onClick={() => logout()}
                className="flex items-center opacity-75 hover:opacity-100 py-2 pl-4 nav-item"
              >
                <i className="fas fa-sign-out-alt mr-3"></i>
                Sair
              </button>
            </nav>
            <button
              onClick={() => setCreateUserIsOpen(!createUserIsOpen)}
              className="w-full bg-white cta-btn font-semibold py-2 mt-5 rounded-lg shadow-lg hover:shadow-xl hover:bg-gray-300 flex items-center justify-center"
            >
              <i className="fas fa-plus mr-3"></i> Cadastrar cliente
            </button>
          </header>

          <div className="w-full overflow-x-hidden border-t flex flex-col">
            <main className="w-full flex-grow p-6">{children}</main>
            <footer className="w-full bg-white text-right p-4">
              Feito por{" "}
              <a
                target="_blank"
                href="https://conselt.com.br"
                className="underline"
              >
                CONSELT
              </a>
              .
            </footer>
          </div>
        </div>
      </div>
    </>
  );
}
