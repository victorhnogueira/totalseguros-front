import Router from 'next/router'

export function logout() {
  const tstoken = localStorage.getItem('ts-token')
  if (tstoken) {
    localStorage.removeItem('ts-token')
  }
  Router.replace('/login')
}
