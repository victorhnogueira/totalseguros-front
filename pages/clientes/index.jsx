import Head from "next/head";
import Link from "next/link";
import WithPrivateRoute from "../../components/WithPrivateRoute";
import useFetch from "../../utils/useFetch";
import { useEffect } from "react";

function Clientes({ userAuth }) {
  const { auth } = userAuth;

  const { data: clientes, mutate: clientesMutate } = useFetch({
    method: "GET",
    baseURL: "https://totalseguros-api.herokuapp.com",
    url: "/user/clients",
    headers: {
      Authorization: `Bearer ${auth}`,
      "Content-Type": "application/json",
    },
  });

  return (
    <div>
      <Head>
        <title>Clientes - Total Seguros</title>
        <meta name="description" content="Gestão Total Seguros" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1 className="text-3xl text-black pb-6">Clientes</h1>
      <div className="w-full mt-12">
        <p className="text-xl pb-3 flex items-center">
          <i className="fas fa-list mr-3"></i> Clientes cadastrados
        </p>
        <div className="bg-white overflow-auto">
          <table className="min-w-full bg-white">
            <thead className="bg-gray-800 text-white">
              <tr>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Nome
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  CPF/CNPJ
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Telefone
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  E-mail
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Ações
                </th>
              </tr>
            </thead>
            <tbody className="text-gray-700">
              {clientes?.map((cliente) => {
                return (
                  <tr key={cliente?.id}>
                    <td className="text-left py-3 px-4">{cliente?.fullName}</td>
                    <td className="text-left py-3 px-4">{cliente?.cpfCnpj}</td>
                    <td className="text-left py-3 px-4">{cliente?.phone}</td>
                    <td className="text-left py-3 px-4">{cliente?.email}</td>
                    <td className="text-left py-3 px-4">
                      <Link href={`/clientes/${cliente?.id}`}>
                        <a
                          href={`/clientes/${cliente?.id}`}
                          className="px-3 py-2 rounded text-white text-xs uppercase bg-indigo-500 hover:bg-indigo-600"
                        >
                          Detalhes
                        </a>
                      </Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

const ClientesPage = WithPrivateRoute(Clientes);
ClientesPage.layout = "admin";

export default ClientesPage;
