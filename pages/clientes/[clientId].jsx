import { Fragment, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import { Dialog, Transition } from "@headlessui/react";
import { XIcon } from "@heroicons/react/outline";
import WithPrivateRoute from "../../components/WithPrivateRoute";
import useFetch from "../../utils/useFetch";

function SideMenuCreateSeguro({ open, setOpen, clientId, auth }) {
  const [isLoadingRequest, setIsLoadingRequest] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const [veiculo, setVeiculo] = useState("");
  const [placa, setPlaca] = useState("");
  const [seguradora, setSeguradora] = useState("");
  const [centralDeAtendimento, setCentralDeAtendimento] = useState("");
  const [numeroApolice, setNumeroApolice] = useState("");
  const [franquia, setFranquia] = useState("");
  const [danosMateriais, setDanosMateriais] = useState("");
  const [danosCorporais, setDanosCorporais] = useState("");
  const [danosMorais, setDanosMorais] = useState("");
  const [carroReserva, setCarroReserva] = useState("");

  const router = useRouter();

  function createSeguro(e) {
    e.preventDefault();

    setIsLoadingRequest(true);

    const axiosInstance = axios.create({
      baseURL: "https://totalseguros-api.herokuapp.com",
      headers: {
        Authorization: `Bearer ${auth}`,
        "Content-Type": "application/json",
      },
    });

    axiosInstance({
      method: "POST",
      url: "/user/segurosauto",
      data: {
        clientId: Number(clientId),
        veiculo,
        placa,
        seguradora,
        centralDeAtendimento,
        numeroApolice,
        franquia,
        danosMateriais,
        danosCorporais,
        danosMorais,
        carroReserva,
      },
    })
      .then(() => {
        setOpen(false);
        router.push("/seguros");
      })
      .catch(() => {
        setErrorMessage("erro ao cadastrar");
      })
      .finally(() => {
        setVeiculo("");
        setPlaca("");
        setCentralDeAtendimento("");
        setNumeroApolice("");
        setFranquia("");
        setDanosMateriais("");
        setDanosCorporais("");
        setDanosMorais("");
        setCarroReserva("");
      });
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed inset-0 overflow-hidden z-50"
        open={open}
        onClose={setOpen}
      >
        <div className="absolute inset-0 overflow-hidden">
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>
          <div className="fixed inset-y-0 right-0 pl-10 max-w-full flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="relative w-screen max-w-md">
                <Transition.Child
                  as={Fragment}
                  enter="ease-in-out duration-500"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in-out duration-500"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <div className="absolute top-0 left-0 -ml-8 pt-4 pr-2 flex sm:-ml-10 sm:pr-4">
                    <button
                      className="rounded-md text-gray-300 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                      onClick={() => setOpen(false)}
                    >
                      <span className="sr-only">Close panel</span>
                      <XIcon className="h-6 w-6" aria-hidden="true" />
                    </button>
                  </div>
                </Transition.Child>
                <div className="h-full flex flex-col py-6 bg-white shadow-xl overflow-y-scroll">
                  <div className="px-4 sm:px-6">
                    <Dialog.Title className="text-lg font-medium text-gray-900">
                      Novo Seguro
                    </Dialog.Title>
                  </div>
                  <div className="mt-6 relative flex-1 px-4 sm:px-6">
                    {/* Replace with your content */}
                    <div className="absolute inset-0 px-4 sm:px-6">
                      {!!errorMessage && (
                        <p className="px-4 py-2 text-red-500">{errorMessage}</p>
                      )}
                      <form
                        id="createSeguroForm"
                        className="space-y-2"
                        onSubmit={(e) => createSeguro(e)}
                      >
                        <div>
                          <label
                            htmlFor="inputVeiculo"
                            className="mb-1 text-xs text-gray-500"
                          >
                            Veiculo
                          </label>
                          <input
                            id="inputVeiculo"
                            className="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker"
                            type="text"
                            name="veiculo"
                            placeholder="Veiculo"
                            onChange={(e) => setVeiculo(e.target.value)}
                            value={veiculo}
                            required
                          />
                        </div>
                        <div>
                          <label
                            htmlFor="inputPlaca"
                            className="mb-1 text-xs text-gray-500"
                          >
                            Placa
                          </label>
                          <input
                            id="inputPlaca"
                            className="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker"
                            type="text"
                            name="placa"
                            placeholder="ABC-1234"
                            onChange={(e) => setPlaca(e.target.value)}
                            value={placa}
                            required
                          />
                        </div>
                        <div>
                          <label
                            htmlFor="inputSeguradora"
                            className="mb-1 text-xs text-gray-500"
                          >
                            Seguradora
                          </label>
                          <input
                            id="inputSeguradora"
                            className="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker"
                            type="text"
                            name="seguro"
                            placeholder="Nome da Seguradora"
                            onChange={(e) => setSeguradora(e.target.value)}
                            value={seguradora}
                            required
                          />
                        </div>
                        <div>
                          <label
                            htmlFor="inputNumApolice"
                            className="mb-1 text-xs text-gray-500"
                          >
                            Numero da Apolice
                          </label>
                          <input
                            id="inputNumApolice"
                            className="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker"
                            type="text"
                            name="apolice"
                            placeholder="Numero da Apolice"
                            onChange={(e) => setNumeroApolice(e.target.value)}
                            value={numeroApolice}
                            required
                          />
                        </div>
                        <div>
                          <label
                            htmlFor="inputCentralAtendimento"
                            className="mb-1 text-xs text-gray-500"
                          >
                            Central de atendimento
                          </label>
                          <input
                            id="inputCentralAtendimento"
                            className="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker"
                            type="text"
                            name="centralatendimento"
                            placeholder="Central de atendimento"
                            onChange={(e) =>
                              setCentralDeAtendimento(e.target.value)
                            }
                            value={centralDeAtendimento}
                            required
                          />
                        </div>
                        <div>
                          <label
                            htmlFor="inputFranquia"
                            className="mb-1 text-xs text-gray-500"
                          >
                            Franquia
                          </label>
                          <input
                            id="inputFranquia"
                            className="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker"
                            type="text"
                            name="franquia"
                            placeholder="Franquia"
                            onChange={(e) => setFranquia(e.target.value)}
                            value={franquia}
                            required
                          />
                        </div>
                        <button
                          type="submit"
                          form="createSeguroForm"
                          className="flex items-center justify-center w-full px-4 py-2 mt-10 text-sm text-white rounded-md bg-indigo-700 hover:bg-indigo-900 focus:outline-none focus:ring focus:ring-purple-700 focus:ring-offset-1 focus:ring-offset-white dark:focus:ring-offset-dark"
                          disabled={isLoadingRequest}
                        >
                          <span aria-hidden="true">
                            {isLoadingRequest && (
                              <svg
                                className="w-4 h-4 mr-2 animate-spin"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                              >
                                <path
                                  fillRule="evenodd"
                                  d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z"
                                  clipRule="evenodd"
                                />
                              </svg>
                            )}
                          </span>
                          <span>Cadastrar Seguro</span>
                        </button>
                      </form>
                    </div>
                    {/* /End replace */}
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

const ClientInfo = ({ userAuth }) => {
  const [createSeguroOpen, setCreateSeguroOpen] = useState(false);
  const router = useRouter();

  const { auth } = userAuth;
  const { clientId } = router.query;

  const { data: cliente } = useFetch({
    method: "GET",
    baseURL: "https://totalseguros-api.herokuapp.com",
    url: `/user/clients/${clientId}`,
    headers: {
      Authorization: `Bearer ${auth}`,
      "Content-Type": "application/json",
    },
  });

  if (!cliente) {
    return <>Carregando...</>;
  }

  return (
    <>
      <Link href="/clientes">
        <p className="text-indigo-800 font-semibold px-4 py-2 my-2 cursor-pointer">
          &#8617; Voltar para todos os clientes
        </p>
      </Link>
      <div className="bg-white shadow overflow-hidden sm:rounded-lg">
        <div className="px-4 py-5 sm:px-6">
          <h3 className="text-lg leading-6 font-medium text-gray-900">
            Cliente
          </h3>
        </div>
        <div className="border-t border-gray-200">
          <dl>
            <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Nome</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {cliente?.fullName}
              </dd>
            </div>
            <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">CPF/CNPJ</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {cliente?.cpfCnpj}
              </dd>
            </div>
            <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">E-mail</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {cliente?.email}
              </dd>
            </div>
            <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Telefone</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {cliente?.phone}
              </dd>
            </div>
            <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt className="text-sm font-medium text-gray-500">Detalhes</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                <ul className="border border-gray-200 rounded-md divide-y divide-gray-200">
                  <li className="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
                    <div className="w-0 flex-1 flex items-center">
                      <span className="ml-2 flex-1 w-0 truncate">
                        Criado em:
                      </span>
                    </div>
                    <div className="ml-4 flex-shrink-0">
                      {cliente?.createdAt}
                    </div>
                  </li>
                  <li className="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
                    <div className="w-0 flex-1 flex items-center">
                      <span className="ml-2 flex-1 w-0 truncate">
                        Atualizado em:
                      </span>
                    </div>
                    <div className="ml-4 flex-shrink-0">
                      {cliente?.updatedAt}
                    </div>
                  </li>
                </ul>
              </dd>
            </div>
          </dl>
        </div>
      </div>
      <div className="w-full mt-12">
        <p className="text-xl pb-3 flex items-center justify-between">
          <span>
            <i className="fas fa-list mr-3"></i> Seguros Contratados
          </span>
          <button
            onClick={() => setCreateSeguroOpen(!createSeguroOpen)}
            className="text-sm bg-indigo-700 hover:bg-indigo-800 text-white px-2 py-1 rounded-md"
          >
            NOVO SEGURO
          </button>
        </p>
        <div className="bg-white overflow-auto">
          <table className="min-w-full bg-white">
            <thead className="bg-gray-800 text-white">
              <tr>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Veiculo
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Placa
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Seguradora
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Apolice
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Ações
                </th>
              </tr>
            </thead>
            <tbody className="text-gray-700">
              {cliente?.seguroAuto?.map((seguro) => (
                <tr key={seguro.id} className="text-xs">
                  <td className="text-left py-3 px-4">{seguro?.veiculo}</td>
                  <td className="text-left py-3 px-4">{seguro?.placa}</td>
                  <td className="text-left py-3 px-4">{seguro?.seguradora}</td>
                  <td className="text-left py-3 px-4">
                    {seguro?.numeroApolice}
                  </td>
                  <td className="text-left py-3 px-4">
                    <Link href={`/seguros/auto/${seguro.id}`}>
                      <a
                        href={`/seguros/auto/${seguro.id}`}
                        className="bg-indigo-700 hover:bg-indigo-800 text-white px-4 py-2 rounded-md"
                      >
                        DETALHES
                      </a>
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <SideMenuCreateSeguro
        open={createSeguroOpen}
        setOpen={setCreateSeguroOpen}
        auth={auth}
        clientId={clientId}
      />
    </>
  );
};

const ClienteInfoPage = WithPrivateRoute(ClientInfo);
ClienteInfoPage.layout = "admin";

export default ClienteInfoPage;
