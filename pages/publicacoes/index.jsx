import Head from "next/head";
import Link from "next/link";
import WithPrivateRoute from "../../components/WithPrivateRoute";
import useFetch from "../../utils/useFetch";
import { useEffect } from "react";

function Clientes({ userAuth }) {
  const { auth } = userAuth;

  const { data: news } = useFetch({
    method: "GET",
    baseURL: "https://totalseguros-api.herokuapp.com",
    url: "/news",
    headers: {
      Authorization: `Bearer ${auth}`,
      "Content-Type": "application/json",
    },
  });

  return (
    <div>
      <Head>
        <title>Clientes - Total Seguros</title>
        <meta name="description" content="Gestão Total Seguros" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1 className="text-3xl text-black pb-6">Publicações</h1>
      <div className="w-full mt-12">
        <p className="text-xl pb-3 flex items-center">
          <i className="fas fa-list mr-3"></i> Posts publicados
        </p>
        <div className="bg-white overflow-auto">
          <table className="min-w-full bg-white">
            <thead className="bg-gray-800 text-white">
              <tr>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Titulo
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Conteudo
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Author
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Imagem
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Link
                </th>
                <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                  Ações
                </th>
              </tr>
            </thead>
            <tbody className="text-gray-700">
              {news?.map((post) => {
                return (
                  <tr key={post?.id}>
                    <td className="text-left py-3 px-4">
                      <p className="text-sm line-clamp-2">{post?.title}</p>
                    </td>
                    <td className="text-left py-3 px-4">
                      <p className="text-sm line-clamp-2">{post?.content}</p>
                    </td>
                    <td className="text-left py-3 px-4">
                      <p className="text-sm line-clamp-2">{post?.userId}</p>
                    </td>
                    <td className="text-left py-3 px-4">
                      {!!post?.image ? (
                        <a
                          href={post?.image}
                          className="text-sm text-indigo-500 underline"
                          target="_blank"
                        >
                          ver imagem
                        </a>
                      ) : (
                        <p className="text-sm">sem imagem</p>
                      )}
                    </td>
                    <td className="text-left py-3 px-4">
                      {!!post?.cta ? (
                        <a
                          href={post?.cta}
                          className="text-sm text-indigo-500 underline"
                          target="_blank"
                        >
                          Abrir link
                        </a>
                      ) : (
                        <p className="text-sm">sem link</p>
                      )}
                    </td>
                    <td className="text-left py-3 px-4">
                      <Link href={`/publicacoes/${post?.id}`}>
                        <a
                          href={`/publicacoes/${post?.id}`}
                          className="px-3 py-2 rounded text-white text-xs uppercase bg-indigo-500 hover:bg-indigo-600"
                        >
                          Detalhes
                        </a>
                      </Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

const ClientesPage = WithPrivateRoute(Clientes);
ClientesPage.layout = "admin";

export default ClientesPage;
