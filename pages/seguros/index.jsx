import { useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import WithPrivateRoute from "../../components/WithPrivateRoute";
import useFetch from "../../utils/useFetch";

function Seguros({ userAuth }) {
  const { auth } = userAuth;

  const { data: segurosAuto } = useFetch({
    method: "GET",
    baseURL: "https://totalseguros-api.herokuapp.com",
    url: "/user/segurosauto",
    headers: {
      Authorization: `Bearer ${auth}`,
      "Content-Type": "application/json",
    },
  });

  return (
    <>
      <div>
        <div className="w-full mt-12">
          <p className="text-xl pb-3 flex items-center">
            <i className="fas fa-list mr-3"></i> Seguros
          </p>
          <div className="bg-white overflow-auto">
            <table className="min-w-full bg-white">
              <thead className="bg-gray-800 text-white">
                <tr>
                  <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                    Nome
                  </th>
                  <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                    CPF/CNPJ
                  </th>
                  <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                    Veiculo
                  </th>
                  <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                    Placa
                  </th>
                  <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                    Seguradora
                  </th>
                  <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                    Apolice
                  </th>
                  <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
                    Ações
                  </th>
                </tr>
              </thead>
              <tbody className="text-gray-700">
                {segurosAuto?.map((seguro) => (
                  <tr key={seguro.id} className="text-xs">
                    <td className="text-left py-3 px-4">
                      {seguro?.client?.fullName}
                    </td>
                    <td className="text-left py-3 px-4">
                      {seguro?.client?.cpfCnpj}
                    </td>
                    <td className="text-left py-3 px-4">{seguro?.veiculo}</td>
                    <td className="text-left py-3 px-4">{seguro?.placa}</td>
                    <td className="text-left py-3 px-4">
                      {seguro?.seguradora}
                    </td>
                    <td className="text-left py-3 px-4">
                      {seguro?.numeroApolice}
                    </td>
                    <td className="text-left py-3 px-4">
                      <Link href={`/seguros/auto/${seguro.id}`}>
                        <a href={`/seguros/auto/${seguro.id}`} className="bg-indigo-700 hover:bg-indigo-800 text-white px-4 py-2 rounded-md">
                          DETALHES
                        </a>
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}

const SegurosPage = WithPrivateRoute(Seguros);
SegurosPage.layout = "admin";

export default SegurosPage;

