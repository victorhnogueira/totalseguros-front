import { useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import WithPrivateRoute from "../components/WithPrivateRoute";
import useFetch from "../utils/useFetch";

function Home({ userAuth }) {
  const { auth } = userAuth;

  return (
    <div>
      <Head>
        <title>Total Seguros</title>
        <meta name="description" content="Gestão Total Seguros" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1 className="text-3xl text-black pb-6">Dashboard</h1>
    </div>
  );
}

const HomePage = WithPrivateRoute(Home);
HomePage.layout = "admin";

export default HomePage;
