import { useState, useEffect } from 'react'
import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'
import axios from 'axios'

const checkUserAuthentication = () => {
  const ISSERVER = typeof window === 'undefined'

  if (!ISSERVER) {
    const tstoken = localStorage.getItem('ts-token')

    if (tstoken) {
      const queryString = window.location.search
      const urlParams = new URLSearchParams(queryString)

      const goto = urlParams.get('redirect')

      if (goto) {
        Router.replace(goto)
      } else {
        Router.replace('/')
      }
    }
  }
}

const Login = () => {
  const [loadingRequest, setLoadingRequest] = useState(false)
  const [serverFeedback, setServerFeedback] = useState({
    error: false,
    msg: ''
  })
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  function loginAttempt(event) {
    event.preventDefault()

    setLoadingRequest(true)
    const axiosInstance = axios.create({
      baseURL: 'https://totalseguros-api.herokuapp.com'
    })

    axiosInstance({
      method: 'post',
      url: '/auth/user/login',
      data: {
        email,
        password
      }
    })
      .then(response => {
        localStorage.setItem('ts-token', response.data.token)
        Router.replace('/')
      })
      .catch((error) => {
        console.log(error.response)
        setServerFeedback({ error: true, msg: 'E-mail e/ou senha incorretos' })
      })
      .finally(() => {
        setLoadingRequest(false)
      })
  }

  useEffect(() => {
    checkUserAuthentication()
  }, [])

  return (
    <>
      <Head>
        <title>Login - Total Seguros</title>
      </Head>
      <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <h1 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
              Total Seguros
            </h1>
          </div>
          <form
            onSubmit={e => loginAttempt(e)}
            className="mt-8 space-y-6"
            action="#"
            method="POST"
          >
            <input type="hidden" name="remember" value="true" />
            <div className="rounded-md shadow-sm -space-y-px">
              <div>
                <label htmlFor="email-address" className="sr-only">
                  E-mail
                </label>
                <input
                  id="email-address"
                  name="email"
                  type="email"
                  autoComplete="email"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="E-mail"
                  onChange={e => setEmail(e.target.value)}
                  value={email}
                />
              </div>
              <div>
                <label htmlFor="password" className="sr-only">
                  Senha
                </label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Senha"
                  onChange={e => setPassword(e.target.value)}
                  value={password}
                />
              </div>
            </div>

            <div>
              <button
                type="submit"
                className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                disabled={loadingRequest}
              >
                {!loadingRequest && (
                  <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                    <svg
                      className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        fillRule="evenodd"
                        d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </span>
                )}
                {loadingRequest && (
                  <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                    <svg
                      className="h-5 w-5 animate-spin text-gray-100 group-hover:text-indigo-400"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </span>
                )}
                Entrar
              </button>
            </div>
          </form>
          {serverFeedback.error && (
            <p className="py-2 px-4 font-medium rounded bg-red-100 text-red-600">
              {serverFeedback.msg}
            </p>
          )}
        </div>
      </div>
    </>
  )
}

export default Login
