import Router from 'next/router'
import jwt from 'jsonwebtoken'
const goto = '/login?redirect=' // Define your login route address.

/**
 * Check user authentication and authorization
 * It depends on you and your auth service provider.
 * @returns {{auth: null}}
 */
const checkUserAuthentication = () => {
  const ISSERVER = typeof window === 'undefined'

  if (!ISSERVER) {
    const tstoken = localStorage.getItem('ts-token')

    const decodedToken = jwt.decode(tstoken)
    const currentDate = new Date()

    // JWT exp is in seconds
    if (decodedToken.exp * 1000 < currentDate.getTime()) {
      localStorage.removeItem('ts-token')
      return { auth: null }
    } else {
      return { auth: tstoken }
    }
  }
}

// eslint-disable-next-line
const WrappedComponent = WrappedComponent => {
  const hocComponent = ({ ...props }) => <WrappedComponent {...props} />

  hocComponent.getInitialProps = async context => {
    const userAuth = await checkUserAuthentication()

    // Are you an authorized user or not?
    if (!userAuth?.auth) {
      // Handle server-side and client-side rendering.
      if (context.res) {
        context.res?.writeHead(302, {
          Location: goto + context.asPath
        })
        context.res?.end()
      } else {
        Router.replace(goto + context.asPath)
      }
    } else if (WrappedComponent.getInitialProps) {
      const wrappedProps = await WrappedComponent.getInitialProps({
        ...context,
        auth: userAuth
      })
      return { ...wrappedProps, userAuth }
    }

    return { userAuth }
  }

  return hocComponent
}

export default WrappedComponent
